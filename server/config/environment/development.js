'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/testcaser-dev'
  },

  // Seed database on startup
  seedDB: true,

  SMTP: {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    debug: true,
    auth: {
      user: 'testcaser.test@gmail.com',
      pass: 'Trinity01'
    }
  }

};
