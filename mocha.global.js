import app from './';
import mongoose from 'mongoose';

after(function(done) {
  app.testCaser.on('close', () => done());
  mongoose.connection.close();
  app.testCaser.close();
});
